"""lotto_estrella URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth.views import login, logout_then_login
from django.contrib.auth.decorators import login_required


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/login/$', login, {'template_name':'login.html'}, name='login'),
    url(r'^$', login_required(TemplateView.as_view(template_name="panel.html")), name='panel'),
    url(r'^logout/$', logout_then_login, name='logout'),
    url(r'^control_usuarios/$', TemplateView.as_view(template_name="herencias.html"), name='tree'),
    url(r'^lista_sucursales/$', TemplateView.as_view(template_name="list_sucursales.html"), name='sucursales'),
    url(r'^crear_sucursales/$', TemplateView.as_view(template_name="crear_sucursal.html"), name='crear_sucursales'),
    url(r'^lista_gestor/$', TemplateView.as_view(template_name="list_gestor.html"), name='gestor'),
    url(r'^crear_gestor/$', TemplateView.as_view(template_name="crear_gestor.html"), name='crear_gestor'),
    url(r'^lista_taquilla/$', TemplateView.as_view(template_name="list_taquilla.html"), name='taquilla'),
    url(r'^crear_taquilla/$', TemplateView.as_view(template_name="crear_taquilla.html"), name='crear_taquilla'),
    url(r'^ajuste_perfil/$', TemplateView.as_view(template_name="profile_bo.html"), name='ajustes'),
    #Resultados
    url(r'^resultados/$', TemplateView.as_view(template_name="result.html"), name='resultados'),
    #Reportes
    url(r'^reportes_generales/$', TemplateView.as_view(template_name="reportes_generales.html"), name='reportes'),
    url(r'^reportes_riesgo/$', TemplateView.as_view(template_name="reporte_riesgo.html"), name='riesgo'),
    #ticketbackoffice
    url(r'^consulta_ticket/$', TemplateView.as_view(template_name="consulta_ticket_BO.html"), name='consulta_ticket'),
    #TicketTaquilla
    #mensajeriageneral
    url(r'^nuevo_mensaje/$', TemplateView.as_view(template_name="new_msg.html"), name='new_msg'),
    url(r'^inbox/$', TemplateView.as_view(template_name="inbox.html"), name='inbox'),
    url(r'^enviados/$', TemplateView.as_view(template_name="send.html"), name='send_msg'),
    url(r'^leer/$', TemplateView.as_view(template_name="read_msg.html"), name='read_msg'),
]
