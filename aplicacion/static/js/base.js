$(function () {


	$('.subnavbar').find ('li').each (function (i) {

		var mod = i % 3;

		if (mod === 2) {
			$(this).addClass ('subnavbar-open-right');
		}

	});



});

//tablas
$(document).ready(function() {
	$('table.varias').DataTable({
		"lengthMenu": [10, 20, 30, 40, 50, 100],
		responsive: true,
		"ordering": false,
		"language": {
			"lengthMenu": "<style>float: left;</style>Resultados por pagina <i class='fa fa-fw fa-list'></i> _MENU_",
			"zeroRecords": "Sin Resultados",
			"info": "Mostrado  _PAGE_ De _PAGES_ Paginas",
			"infoEmpty": "No Hay Resultados",
			"infoFiltered": "(Se muestran from _MAX_ total Resultados)",
			"oPaginate": {
				sFirst: "Primero",
				sLast: "Ultimo",
				sNext: "Siguiente",
				sPrevious: "Atras"
			},
			"sSearch": "Buscar <i class='fa fa-fw fa-search'></i>",
			"sInfoFiltered": "(Busqueda en: _MAX_ Filas)",

		}
	});
});

$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
});

$('#daterange-btn').daterangepicker(
    {
        "locale": {
      "format": "DD/MM/YYYY",
      "separator": " - ",
      "fromLabel": "Desde",
      "toLabel": "Hasta",
      "customRangeLabel": "Perzonalizar Fechas",
      "weekLabel": "W",
      "daysOfWeek": [
          "Do",
          "Lu",
          "Ma",
          "Mi",
          "Ju",
          "Vi",
          "Sa"
      ],
      "monthNames": [
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Septiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
      ],
      "firstDay": 1
      },
      "autoApply": true,
      ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate: moment()
    },
    function (start, end) {
      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
);
//
$(".select2 ").select2();
//
$(function() {
	//Add text editor
	$("#compose-textarea").wysihtml5();
});
